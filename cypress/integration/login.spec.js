describe('When I login', function() {
    beforeEach(() => {
        cy.visit('index.php')

        cy.get('#username').type('finkingma')
        cy.get('#password').type('89WF71')
        cy.get('#login').click() 
      })

    it('I should be able to search hotels', function() {
        cy.get('.login_title').should('contain', 'Search Hotel')
    })
  })