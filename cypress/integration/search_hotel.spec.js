describe('When I search for hotels', function () {
  beforeEach(() => {
    cy.login("finkingma","ZI5128")
    cy.visit('SearchHotel.php')
  })

  it('I should be able to find hotels in Sydney', function () {
    cy.get('#location').select('Sydney')
    cy.get('#hotels').select('Hotel Cornice')
    cy.get('#Submit').click()
    cy.get('.login_title').should('contain', 'Select Hotel')
    cy.get('#hotel_name_1').should('have.value', 'Hotel Cornice')
  })
})